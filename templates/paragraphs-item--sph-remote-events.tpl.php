<?php

/**
 * @file
 * Default theme implementation for a single paragraph item.
 *
 * Available variables:
 * - $content: An array of content items. Use render($content) to print them
 *   all, or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity
 *   - entity-paragraphs-item
 *   - paragraphs-item-{bundle}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>
<div class="single-event">
    <div id="paragraph-<?php print $entity_id; ?>"></div>
  <?php
    print '<div class="single-event-info">';
    print '<a href="' . render($content['field_event_url']) . '">';
    print '<p class="date">' . render($content['field_sph_event_date']) . '</p>';
    print render($content['field_event_title']);
    if(isset($content['field_event_grades'])) {
      print '<p class="info">' . render($content['field_event_grades']) . ', ' . render($content['field_sph_event_cost']) . '</p>';
    }
    else {
      print '<p class="info">' . render($content['field_sph_event_cost']) . '</p>';
    }
    print render($content['field_event_teaser']);
    print '</a>';
    print '</div>';
    if(isset($content['field_sph_event_themes'])) {
      print render($content['field_sph_event_themes']);
    }
  ?>
</div>
