<?php

/**
 * @file
 * Default theme implementation for a single paragraph item.
 *
 * Available variables:
 * - $content: An array of content items. Use render($content) to print them
 *   all, or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity
 *   - entity-paragraphs-item
 *   - paragraphs-item-{bundle}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */

if ($content['field_sph_image_align']['#items'][0]['value'] == 1) {
  $align = "left";
}
else {
  $align = "right";
}
?>

<?php if (isset($content['field_sph_anchor_link']['#items'][0]['value'])) { ?>
  <a id="<?php print $content['field_sph_anchor_link']['#items'][0]['value']; ?>" class="sph-anchor-link-navigation"></a>
<?php } ?>

<?php unset($content['field_sph_anchor_link']); ?>

<div class="captioned-image-wrapper <?php print $align; ?>">
  <div class="captioned-image-text-wrapper">
    <div class="captioned-image-text--inner">
      <?php if (isset($content['field_sph_icon_image'])): ?>
        <div class="captioned-image-text-icon">
          <?php print render($content['field_sph_icon_image']); ?>
        </div>
      <?php endif; ?>
      <div class="captioned-image-text-text">
        <?php print render($content['field_sph_image_text']); ?>
      </div>
    </div>
  </div>
  <div class="captioned-image-image">
    <?php print render($content['field_sph_captioned_image']); ?>
  </div>
  <?php if (isset($content['field_sph_image_text'])): ?>

  <?php endif; ?>
</div>
