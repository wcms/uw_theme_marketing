<?php

/**
 * @file
 * Default theme implementation for a single paragraph item.
 *
 * Available variables:
 * - $content: An array of content items. Use render($content) to print them all
 *   , or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity
 *   - entity-paragraphs-item
 *   - paragraphs-item-{bundle}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>
<div class="single-event">
  <?php
  print '<div class="single-event-info">';
  ?>
    <div class="field_event_date field field-label-hidden clearfix">
        <div class="field-data multiple">
            <?php if(isset($content['field_sph_non_wcms_event_date'][1])):?>
            <input tabindex="-1" id="CheckboxSwitch-<?php print $content['field_sph_non_wcms_event_title'][0]['#markup']?>" type="checkbox" >
            <?php endif; ?>
            <div class="field-item-switch">
              <?php
              foreach ($content['field_sph_non_wcms_event_date'] as $key => $value) :
                if (is_array($value) && isset($value['#markup'])):
                  print '<div class="field-item">' . render($value['#markup']) . '</div>';
                endif;
              endforeach;
              ?>
            </div>
            <?php if(isset($content['field_sph_non_wcms_event_date'][1])):?>
            <div class="switchable">
                <label  id="CheckboxSwitch-<?php print $content['field_sph_non_wcms_event_title'][0]['#markup']?>-L" aria-checked="false" tabindex="0"  for="CheckboxSwitch-<?php print $content['field_sph_non_wcms_event_title'][0]['#markup']?>">
                    <span class="hide">HIDE</span>
                    <span class="show">SHOW ALL</span>
                </label>
            </div>
            <?php endif; ?>
        </div>
    </div>
  <?php
  if (isset($content['field_sph_non_wcms_event_url']) && isset($content['field_sph_non_wcms_event_url']['#items'][0]['url'])):
    print '<a href="' . $content['field_sph_non_wcms_event_url']['#items'][0]['url'] . '">';
  endif;
  print render($content['field_sph_non_wcms_event_title']);
  if (isset($content['field_sph_non_wcms_event_cost']['#items'][0]['value']) && $content['field_sph_non_wcms_event_cost']['#items'][0]['value'] == 0) :
    print '<p class="info">free</p>';
  else :
    print '<p class="info">' . render($content['field_sph_non_wcms_event_cost']) . '</p>';
  endif;
  print render($content['field_sph_non_wcms_event_teaser']);
  if (isset($content['field_sph_non_wcms_event_url']) && isset($content['field_sph_non_wcms_event_url']['#items'][0]['url'])) :
    print '</a>';
  endif;
  print '</div>';
  ?>
</div>
