<?php

/**
 * @file
 * Default theme implementation for a single paragraph item.
 *
 * Available variables:
 * - $content: An array of content items. Use render($content) to print them
 *   all, or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity
 *   - entity-paragraphs-item
 *   - paragraphs-item-{bundle}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
hide($content['field_sph_display_title']);
hide($content['field_sph_block_title']);
?>
<?php if (isset($content['field_sph_anchor_link']['#items'][0]['value'])) { ?>
  <a id="<?php print $content['field_sph_anchor_link']['#items'][0]['value']; ?>" class="sph-anchor-link-navigation"></a>
<?php } ?>

<?php unset($content['field_sph_anchor_link']); ?>

<div class="sph-stories--wrapper">
  <?php if ($content['field_sph_display_title']['#items'][0]['value']): ?>
    <div class="sph-stories--title-wrapper">
      <div class="sph-stories--title">
        <h2 class="uw_utm_h2"><?php print render($content['field_sph_block_title']); ?></h2>
      </div>
    </div>
  <?php endif; ?>
  <div class="sph-stories--content-wrapper">
    <div class="sph-stories--content">
      <?php print render($content); ?>
    </div>
  </div>
</div>
