<?php

/**
 * @file
 * Default theme implementation for a single paragraph item.
 *
 * Available variables:
 * - $content: An array of content items. Use render($content) to print them
 *   all, or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity
 *   - entity-paragraphs-item
 *   - paragraphs-item-{bundle}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
// dsm($content);
/*
$banner_small = image_style_url('uw_homepage_banners', $file);
$banner_square = image_style_url('uw_homepage_banners_small', $file);
$banner_med = image_style_url('uw_homepage_banners_medium', $file);
$banner_large = image_style_url('uw_homepage_banners_large', $file);
$banner_xl = image_style_url('uw_homepage_banners_xl', $file);
*/
if (isset($content['field_sph_heading_image'])) {
  $file = $content['field_sph_heading_image']['#items'][0]['uri'];
  $banner_xsmall = image_style_url('sph_header_image_xsmall', $file);
  $banner_small = image_style_url('sph_header_image_small', $file);
  $banner_med = image_style_url('sph_header_image_medium', $file);
  $banner_large = image_style_url('sph_header_image_large', $file);
  $banner_xlarge = image_style_url('sph_header_image_xlarge', $file);
}

hide($content['field_sph_display_field']);
?>

<?php if (isset($content['field_sph_anchor_link']['#items'][0]['value'])) { ?>
  <a id="<?php print $content['field_sph_anchor_link']['#items'][0]['value']; ?>" class="sph-anchor-link-navigation"></a>
<?php } ?>

<?php unset($content['field_sph_anchor_link']); ?>

<div class="sph-heading-block--wrapper">
  <div class="sph-heading-block--inner">
    <div class="sph-heading-block--title">
      <h3>
        <?php print render($content['field_sph_block_title']); ?>
      </h3>
    </div>
    <div class="sph-heading-block--display-wrapper <?php if (isset($content['field_sph_heading_image'])) {print 'with-image';
   } ?>">
      <?php if (isset($content['field_sph_icon_image'])) { ?>
        <div class="heading-icon">
          <?php print render($content['field_sph_icon_image']); ?>
        </div>
      <?php } ?>
      <?php if (isset($content['field_sph_heading_image'])) { ?>
        <div class="heading-image">
          <picture>
            <source srcset="<?php print $banner_xlarge; ?>" media="(min-width: 1024px)">
            <source srcset="<?php print $banner_large; ?>" media="(min-width: 769px)">
            <source srcset="<?php print $banner_med; ?>" media="(min-width: 480px)">
            <source srcset="<?php print $banner_small; ?>" media="(min-width: 320px)">
            <source srcset="<?php print $banner_small; ?>">
            <img src="<?php print $banner_large; ?>" alt="<?php print $content['field_sph_icon_image']['#items'][0]['alt']; ?>">
          </picture>
        </div>
      <?php } else { ?>
        <?php if (isset($content['field_sph_ff_id']['#items'][0]['value'])) {?>
          <div class="sph-heading-block--facts-wrapper">
            <div class="heading-content">
              <?php print render($content['field_sph_ff_id']); ?>
            </div>
          </div>
        <?php } ?>
      <?php } ?>
    </div>
    <?php if (isset($content['field_sph_ff_id']['#items'][0]['value']) && isset($content['field_sph_heading_image'])) {?>
        <div class="sph-heading-block--facts-wrapper">
          <div class="heading-content">
            <?php print render($content['field_sph_ff_id']); ?>
          </div>
        </div>
    <?php } ?>
  </div>
</div>
