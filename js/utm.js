/**
 * @file
 */

(function ($) {
  $(window).load(function isIe() {
    var ua = window.navigator.userAgent;
    var oldIe = ua.indexOf('MSIE ');
    var newIe = ua.indexOf('Trident/');
    if ((oldIe > -1) || (newIe > -1)) {
      $('html').addClass('isie-old');
    }
  });

  // Add event listener on window resize for video loading.
  window.addEventListener('resize', videoLoad);

  // Ensuring that video does not load at smaller screen sizes.
  function videoLoad() {

    var screenWidth = $(window).width();

    if (screenWidth < 769) {
      $('video').removeAttr('autoplay');
      $('video').attr('preload', 'none');
    }
    else {
      $('video').attr('autoplay', 'true');
      $('video').removeAttr('preload');
    }
  }

  $(document).ready(function () {

    // Ensuring that video does not load at smaller screen sizes.
    videoLoad();

    //clicking play/pause button (uploaded videos)
      $('.videoWrapper .embedded-controls .video-control').click(function () {
          if($(".videoWrapper video").get(0).paused){
              $(".videoWrapper video").get(0).play();
              $(this).removeClass("play");
              $(this).addClass("pause");
              $(this).attr('aria-label', 'Pause');
          } else {
              $(".videoWrapper video").get(0).pause();
              $(this).removeClass("pause");
              $(this).addClass("play");
              $(this).attr('aria-label', 'Play');
          }
      });

      //hiding description
      $('.videoWrapper .controls .description-control').click(function () {
          $(".videoWrapper .description").slideToggle();
          $(this).toggleClass("down");
          $(this).toggleClass("up");
      });

    // Third party videos
    // Uses jQuery.mb.YTPlayer library, docs here: https://github.com/pupunzi/jquery.mb.YTPlayer/wiki
    if ($('#thirdparty-banner').length > 0) {

      var iframe = $('#thirdparty-banner iframe');
      var player = new Vimeo.Player(iframe);
      // Thirdparty video controls
      $('.thirdparty-controls .video-control').click(function() {
        if ($(this).hasClass('pause')) {
          player.pause();
        } else {
          player.play();
        }
        $(this).toggleClass('play').toggleClass('pause');
      });

    }

    // ****** COPY BLOCK ******.
    $('.field-name-field-sph-copy > .expandable > h2:first-child').click(function () {
      $expandable = $(this).parent();
      if ($expandable.hasClass('expanded')) {
        $expandable.removeClass('expanded');
        $('.expandable-content',$expandable).prev().addClass('last-visible');
      }
      else {
        $expandable.addClass('expanded');
        $('.expandable-content',$expandable).prev().removeClass('last-visible');
      }
    }).wrapInner('<button>');

    // Add last-visible class to the last visible item before the hidden content, and the last item in the hidden content, to remove lower margins
    // .node-type-uw-web-page.
    $('.field-name-field-sph-copy > .expandable > .expandable-content').prev().addClass('last-visible');

    // .node-type-uw-web-page.
    $('.field-name-field-sph-copy > .expandable > .expandable-content :last-child').addClass('last-visible');

    // If there is more than one expandable region on the page, add expand/collapse all functions.
    if ($('.field-name-field-sph-copy > .expandable').length > 1) {

      // .node-type-uw-web-page.
      $('.field-name-field-sph-copy > .expandable:first').before('<div class="expandable-controls"><button class="expand-all">Expand all</button><button class="collapse-all">Collapse all</button></div>');

      $('#content .expandable-controls .expand-all').click(function () {
          // Rather than recreate clicking logic here, just click the affected items.
        $('#content .expandable:not(.expanded) h2:first-child').click();
      });

      $('#content .expandable-controls .collapse-all').click(function () {

        // Rather than recreate clicking logic here, just click the affected items.
        $('#content .expandable.expanded h2:first-child').click();
      });
    }

    // ****** BODY SINGLE PAGE ******.
    $('.field-name-body > .expandable > h2:first-child').click(function () {
      $expandable = $(this).parent();

      if ($expandable.hasClass('expanded')) {
        $expandable.removeClass('expanded');
        $('.expandable-content',$expandable).prev().addClass('last-visible');
      }
      else {
        $expandable.addClass('expanded');
        $('.expandable-content',$expandable).prev().removeClass('last-visible');
      }
    }).wrapInner('<button>');

    // Add last-visible class to the last visible item before the hidden content, and the last item in the hidden content, to remove lower margins
    // .node-type-uw-web-page.
    $('.field-name-body > .expandable > .expandable-content').prev().addClass('last-visible');

    // .node-type-uw-web-page.
    $('.field-name-body > .expandable > .expandable-content :last-child').addClass('last-visible');

    // If there is more than one expandable region on the page, add expand/collapse all functions.
    if ($('.field-name-body > .expandable').length > 1) {
      // .node-type-uw-web-page.
      $('.field-name-body > .expandable:first').before('<div class="expandable-controls"><button class="expand-all">Expand all</button><button class="collapse-all">Collapse all</button></div>');

      $('#content .expandable-controls .expand-all').click(function () {
        // Rather than recreate clicking logic here, just click the affected items.
        $('#content .expandable:not(.expanded) h2:first-child').click();
      });

      $('#content .expandable-controls .collapse-all').click(function () {
          // Rather than recreate clicking logic here, just click the affected items.
          $('#content .expandable.expanded h2:first-child').click();
      });
    }

    // Do things when the anchor changes.
    window.onhashchange = uw_fdsu_anchors;

    // Trigger the event on page load in case there is already an anchor.
    uw_fdsu_anchors();

    // Function to do things when the anchor changes.
    function uw_fdsu_anchors() {
      if (location.hash) {
        // Check if there is an ID with this name.
        if ($(location.hash).length) {
          // If it's in an unexpanded expandable content area, expand the expandable content area.
          $(location.hash,'.expandable:not(.expanded)').closest('.expandable').find('h2:first-child').click();
            // Scroll to the ID, taking into account the toolbar if it exists
            // "html" works in Firefox, "body" works in Chrome/Safari.
            $('html, body').scrollTop($(location.hash).offset().top - $('#toolbar').height());
          }
        }
      }
    });
})(jQuery);
(function labelClick($, window, document) {
  'use strict';
  function setFocusToTextBox(id){
    document.getElementById(id).focus();
  }
  function handleDocumentReady() {
    $("input[id^='CheckboxSwitch']").each(function () {
      $('label[for="' + $(this).attr('id') + '"]').on('keydown', function (e) {
        var code = e.which;
        if ((code === 13) || (code === 32)) {
          $(this).click();
          setFocusToTextBox($(this).attr('id'));
        }
      });
    });
  }
  $(document).ready(handleDocumentReady);
})(window.jQuery, window, window.document);
