<?php

/**
 * @file
 * Themes field collection items printed using field_collection_view formatter.
 */

/**
 * Implements template_preprocess_block().
 */
function uw_theme_marketing_preprocess_block(&$variables) {
  // Add @aria-label to responsive_menu_combined block.
  if (!empty($variables['block']->bid) && $variables['block']->bid === 'responsive_menu_combined-responsive_menu_combined') {
    $variables['attributes_array']['aria-label'] = 'Main';
  }
}

/**
 * Implements hook_css_alter().
 */
function uw_theme_marketing_css_alter(&$css) {
  // We only loaded the core theme for its functions, we don't want its CSS.
  $path = drupal_get_path('theme', 'uw_fdsu_theme_resp') . '/';
  foreach ($css as $key => $source) {
    if (substr($key, 0, strlen($path)) == $path) {
      unset($css[$key]);
    }
  }
  $path_ctools = drupal_get_path('module', 'ctools') . '/css/';
  $path_global_footer = drupal_get_path('module', 'uw_nav_global_footer') . '/css/';
  $path_site_footer = drupal_get_path('module', 'uw_nav_site_footer') . '/css/';
  $path_system_theme = drupal_get_path('module', 'system') . '/';
  $path_cta = drupal_get_path('module', 'uw_ct_embedded_call_to_action') . '/css/';
  $path_site_share = drupal_get_path('module', 'uw_social_media_sharing') . '/css/';
  $path_ff = drupal_get_path('module', 'uw_ct_embedded_facts_and_figures') . '/css/';

  $exclude = [
    $path_ctools . 'modal.css' => TRUE,
    $path_global_footer . 'uw_nav_global_footer.css' => TRUE,
    $path_cta . 'uw_ct_embedded_call_to_action.css' => TRUE,
    $path_system_theme . 'system.base.css' => TRUE,
    $path_system_theme . 'system.theme.css' => TRUE,
    $path_site_footer . 'uw_nav_site_footer.css' => TRUE,
    $path_site_share . 'uw_social_media_sharing.css' => TRUE,
    $path_ff . 'highlighted_fact.css' => TRUE,
  ];
  $css = array_diff_key($css, $exclude);
}

/**
 * Implements hook_preprocess_html().
 */
function uw_theme_marketing_preprocess_html(&$variables) {
  // Add meta tags to enable mobile view.
  $meta_ie_render_engine = [
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => [
      'http-equiv' => 'X-UA-Compatible',
      'content' => 'IE=edge',
    ],
  ];
  $meta_mobile_view = [
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => [
      'name' => 'viewport',
      'content' => 'width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0',
    ],
  ];
  $utm_path = drupal_get_path('theme', 'uw_theme_marketing') . '/css/utm.css';
  drupal_add_css($utm_path);
  drupal_add_html_head($meta_ie_render_engine, 'meta_ie_render_engine');
  drupal_add_html_head($meta_mobile_view, 'meta_mobile_view');
}

/**
 * Implements hook_preprocess_node().
 */
function uw_theme_marketing_preprocess_node(&$variables) {
  $node = $variables['node'];
  $nodetype = $node->type;
  if ($nodetype == 'uw_ct_single_page_home') {
    // Create variables to negotiate banner type.
    $banner_type_field = field_get_items('node', $node, 'field_banner_type');
    $banner_type = $banner_type_field[0]['value'];
    $variables['banner_type'] = $banner_type;
    if ($banner_type == 'video_url') {

      // Add the Vimeo SDK.
      drupal_add_js('https://player.vimeo.com/api/player.js', 'external');
      $thirdparty_video_field = field_get_items('node', $node, 'field_third_party_video');
      $video_num = $thirdparty_video_field[0]['value'];

      // Check if the video is valid.
      $video_url = 'https://player.vimeo.com/video/' . $video_num;
      $headers = @get_headers($video_url);

      /* If it's valid, then send the variable */
      if (strpos($headers[0], '200 OK') == TRUE) {
        $variables['vimeo_num'] = $video_num;
      }
      /* otherwise, use the fallback image */
      else {
        $fallback_image_field = field_get_items('node', $node, 'field_banner_fallback_image');
        $variables['fallback_image_uri'] = file_create_url($fallback_image_field[0]['uri']);

        $variables['fallback_image_large'] = image_style_url('sph_header_image_xlarge', $fallback_image_field[0]['uri']);
        $variables['fallback_image_mobile'] = image_style_url('sph_header_image_small', $fallback_image_field[0]['uri']);
      }
    }
  }
}

/**
 * Implements hook_preprocess_entity().
 */
function uw_theme_marketing_preprocess_entity(&$variables) {

  // If a paragraph item and the heading block, continue to process.
  if ($variables['entity_type'] == 'paragraphs_item' && $variables['elements']['#bundle'] == 'sph_heading_block') {

    // If on the display field, continue to process.
    if (isset($variables['field_sph_ff_id'][0]['value'])) {

      // Set the html variable to be used the facts and figures
      // cke_ff_process().
      $html = '<ckfactsfigures data-factsfigures-nid="' . $variables['field_sph_ff_id'][0]['value'] . '" data-numberpercarousel="3" data-usecarousel="Yes"></ckfactsfigures>';

      // Replace the markup with facts and figures output, produced from
      // cke_ff_process.
      $variables['content']['field_sph_ff_id'][0]['#markup'] = cke_ff_process($html, NULL, NULL, NULL, NULL, NULL);
    }
  }

  $entity = $variables['elements']['#entity'];
  $entity_type = $variables['elements']['#entity_type'];
  $variables['entity_id'] = entity_id($entity_type, $entity);
}

/**
 * Implements hook_js_alter().
 */
function uw_theme_marketing_js_alter(&$javascript) {
  $path = drupal_get_path('theme', 'uw_fdsu_theme_resp') . '/';
  if (module_exists('uw_header_search')) {
    $utm_path = drupal_get_path('module', 'uw_header_search') . '/uw_header_search.js';
    drupal_add_js($utm_path);
    drupal_add_js([
      'CToolsModal' => [
        'modalSize' => [
          'type' => 'scale',
          'width' => 1,
          'height' => 1,
        ],
        'modalOptions' => [
          'opacity' => .98,
          'background-color' => '#252525',
        ],
        'animation' => 'fadeIn',
        'animationSpeed' => 'slow',
        'throbberTheme' => 'CToolsThrobber',
        // Customize the AJAX throbber like so: This function assumes the images
        // are inside the module directory's "images" directory:
        'throbber' => theme('image', [
          'path' => ctools_image_path('spacer.png', 'uw_header_search'),
          'alt' => t('Loading...'),
          'title' => t('Loading'),
        ]
        ),
        'closeText' => '',
        'closeImage' => theme('image', [
          'path' => ctools_image_path('spacer.png', 'uw_header_search'),
          'alt' => '',
          'title' => '',
          ]
        ),
      ],
    ], 'setting');
    $exclude = [
      $path . 'scripts/resp.js' => FALSE,
    ];

    $javascript = array_diff_key($javascript, $exclude);
  }
}
